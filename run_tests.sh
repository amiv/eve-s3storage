#!/bin/bash

docker build -t eve-s3storage-testrunner -f Dockerfile.test . || exit 1
docker run --rm -it --name testrunner --mount type=tmpfs,destination=/tmp \
    eve-s3storage-testrunner $@
